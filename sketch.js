var messages = [];

function setup() {
    createCanvas(windowWidth, windowHeight);
    // any additional setup code goes here
    messages.push({
        text: "Hi friend",
        from: "Kaley"
    });
    messages.push({
        text: "Whats up?",
        from: "Me"
    });
    messages.push({
        text: "Nm",
        from: "Kaley"
    });
    messages.push({
        text: "Cool.",
        from: "Me"
    });
}

function draw() {
    // your "draw loop" code goes here
    background(200);

    messages.map(drawMessage);

    if(frameCount%60 == 0) {
        messages.push({
            text: "Sup.",
            from: "Me"
        });

    }
}

function drawMessage( msg, i ) {

    if(msg.from == "Me") {
        textAlign(RIGHT);
        text(msg.text, width-50, 50+ i*30);
    } else {
        textAlign(LEFT);
        text(msg.text, 50, 50+ i*30);
    }

}

